import React, { Component } from 'react'
import "./RenderGlass.css"
export default class RednerGlass extends Component {

    state={
        glassList : [
                {
                    "id": 1,
                    "price": 30,
                    "name": "GUCCI G8850U",
                    "url": "./glassesImage/v1.png",
                    "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
                },
                {
                    "id": 2,
                    "price": 50,
                    "name": "GUCCI G8759H",
                    "url": "./glassesImage/v2.png",
                    "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
                },
                {
                    "id": 3,
                    "price": 30,
                    "name": "DIOR D6700HQ",
                    "url": "./glassesImage/v3.png",
                    "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
                },
                {
                    "id": 4,
                    "price": 70,
                    "name": "DIOR D6005U",
                    "url": "./glassesImage/v4.png",
                    "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
                },
                {
                    "id": 5,
                    "price": 40,
                    "name": "PRADA P8750",
                    "url": "./glassesImage/v5.png",
                    "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
                },
                {
                    "id": 6,
                    "price": 60,
                    "name": "PRADA P9700",
                    "url": "./glassesImage/v6.png",
                    "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
                },
                {
                    "id": 7,
                    "price": 80,
                    "name": "FENDI F8750",
                    "url": "./glassesImage/v7.png",
                    "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
                },
                {
                    "id": 8,
                    "price": 100,
                    "name": "FENDI F8500",
                    "url": "./glassesImage/v8.png",
                    "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
                },
                {
                    "id": 9,
                    "price": 60,
                    "name": "FENDI F4300",
                    "url": "./glassesImage/v9.png",
                    "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
                }
            ] ,
        
       modelGlass:
        {
            "id": 1,
            "price": 30,
            "name": "GUCCI G8850U",
            "url": "./glassesImage/v1.png",
            "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        }
       
        };
        renderDSK =()=>{
         let glassList = this.state.glassList.map((glass)=>{
                return  <button className='btn-glass ' onClick={()=>{
                    this.HandleRenderModelGlass(glass.id)
                }}> <img src = {glass.url} alt="" /></button>
            })
            return glassList;
        }
        
        HandleRenderModelGlass = (idGl)=>{
            var viTri = -1;
           for (let index = 0; index < this.state.glassList.length; index++) {
          var item = this.state.glassList[index];
            if(idGl == item.id){
                viTri = index;
            }
           }
           this.setState({modelGlass:
            {...this.state.item,
                price:this.state.glassList[viTri].price,
                name:this.state.glassList[viTri].name,
                desc:this.state.glassList[viTri].desc,
                url:this.state.glassList[viTri].url,
            }})
                }
  render() {
    return (
        <div >
         <img className="body" src="./glassesImage/background.jpg" alt="" />

    <div className="header">
      <h5 className="py-5">TRY GLASSES APP ONLINE</h5>
    </div>
    <div className="container">
      <div className="model my-5">
        <img src="./glassesImage/model.jpg" alt="" />
      <div className="glass-model">
      <img src={this.state.modelGlass.url} alt="" />
      </div>
      <div id="glassesInfo" class="vglasses__info">
    <h5
     class="info-header"  id="info-header">{this.state.modelGlass.name}</h5>
    <p class="info-body" id="info-body">
        <span id="giaTien">{this.state.modelGlass.price}$</span> 
    </p>
    <p class="info-content" id="info-content">
         {this.state.modelGlass.desc}
     </p>
    </div>
    
      </div>
      <div className="type-glass row" >
      {this.renderDSK()}
      </div>
    </div>
    </div>
   
     
    )
  }
}
